#include <bits/stdc++.h>
using namespace std;
void hanab(int n);
void hanac(int n);
void hanbc(int n);
void hanba(int n);
void hancb(int n);
void hanca(int n);

void hanac(int n)
{
    if(n == 1){
        cout << "a -> c" << endl;
        return ;
    }
    hanab(n-1);
    cout << "a -> c" << endl;
    hanbc(n-1);

}

void hanab(int n)
{
    if(n == 1){
        cout << "a -> b" << endl;
        return ;
    }
    hanac(n-1);
    cout << "a -> b" << endl;
    hancb(n-1);

}
void hanbc(int n)
{
    if(n == 1){
        cout << "b -> c" << endl;
        return ;
    }
    hanba(n-1);
    cout << "b -> c" << endl;
    hanac(n-1);
}

void hanba(int n)
{
    if(n == 1){
        cout << "b -> a" << endl;
        return ;
    }
    hanbc(n-1);
    cout << "b -> a" << endl;
    hanca(n-1);
}

void hanca(int n)
{
    if(n == 1){
        cout << "c -> a" << endl;
        return ;
    }
    hancb(n-1);
    cout << "c -> a" << endl;
    hanba(n-1);
}

void hancb(int n)
{
    if(n == 1){
        cout << "c -> b" << endl;
        return ;
    }
    hanca(n-1);
    cout << "c -> b" << endl;
    hanab(n-1);
}

main()
{
    hanac(3);
    getchar();
    return 0;
}